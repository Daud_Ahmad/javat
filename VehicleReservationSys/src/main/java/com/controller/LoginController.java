package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.service.RegisterServiceIntf;

@Controller
public class LoginController {
	@Autowired
	RegisterServiceIntf service;
	@RequestMapping("/login")
	public ModelAndView login()
	{
		return new ModelAndView("login");
	}
	@RequestMapping("/saveLogin")
	public ModelAndView checkLogin(@RequestParam("firstName") String firstName,@RequestParam("password") String password)
	{
		 boolean status=service.checkLogin(firstName,password);
	        if(status)
	        {
	        	return new ModelAndView("home");
	        }
	        else
	        {
	        	System.out.println("Invalid Details");
	        	return new ModelAndView("login");
	        }
	        
	}

}
