package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Register;

@Repository
public class RegisterDaoImpl implements RegisterDaoIntf{

	@Autowired
	SessionFactory sessionFactory;
	public void saveRegister(Register register) {
		sessionFactory.openSession().save(register);
		
	}
	@Override
	public boolean checkLogin(String firstName, String password) {
		boolean userFound =false;
		String sql="from Register as o where o.firstName=? and password=?";
		Query query =sessionFactory.openSession().createQuery(sql);
		query.setParameter(0,firstName);
		query.setParameter(1,password);
		List list=query.list();
		
		if((list!=null) && (list.size()>0))
		{
			userFound=true;
		}
		return userFound;
	}
	
	}

