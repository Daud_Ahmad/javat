package com.dao;

import java.util.List;

import com.model.Register;

public interface RegisterDaoIntf {

	void saveRegister(Register register);

	boolean checkLogin(String firstName, String password);

}
