package com.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebxmlConfiguration extends AbstractAnnotationConfigDispatcherServletInitializer {

	
	protected Class<?>[] getRootConfigClasses() {
		
		return null;
	}

	
	protected Class<?>[] getServletConfigClasses() {
		
		return new Class[] {
			InternalResourceDataBaseConfiguration.class	
		};
	}

	
	protected String[] getServletMappings() {
		
		return new String [] {
				"/"
		};
	}

}
