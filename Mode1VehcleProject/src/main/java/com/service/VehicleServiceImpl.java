package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.model.Vehicle;

import com.dao.VehicleDAO;

@Service("vehicleService")
@Transactional(propagation=Propagation.SUPPORTS)
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	VehicleDAO vehicleDAO ;

	public Vehicle insertVehicle(Vehicle vehicle) {
		System.out.println("In Service" + vehicle);
		return vehicleDAO.insertVehicle(vehicle);
	}

	public List<Vehicle> getallVehicle() {
		
		return vehicleDAO.getallVehicle();
	}

	public List<Vehicle> searchVehicleByValues(String search_by, String search_value) {

		System.out.println(vehicleDAO.searchVehicleByValues(search_by,search_value));
		return vehicleDAO.searchVehicleByValues(search_by,search_value);
	}

	public Vehicle updateVehicle(Vehicle vehicle) {

		return vehicleDAO.updateVehicle(vehicle);
	}

	public Vehicle sendVehicle(String vehicleNumber) {

		return vehicleDAO.sendVehicle(vehicleNumber);
	}

}
