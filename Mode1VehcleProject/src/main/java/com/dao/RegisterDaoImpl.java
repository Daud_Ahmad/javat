package com.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Register;

@Repository
public class RegisterDaoImpl implements RegisterDaoIntf{

	@Autowired
	SessionFactory sessionFactory;
	public void saveRegister(Register register) {
		sessionFactory.openSession().save(register);
		
	}
	
	}

