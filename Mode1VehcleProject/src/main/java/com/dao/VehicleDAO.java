package com.dao;

import java.util.List;

import com.model.Vehicle;

public interface VehicleDAO {
public Vehicle insertVehicle(Vehicle vehicle);
public List<Vehicle> getallVehicle();
public List<Vehicle> searchVehicleByValues(String search_by, String search_value);
public Vehicle updateVehicle(Vehicle vehicle);
public Vehicle sendVehicle(String vehicleNumber);
}
